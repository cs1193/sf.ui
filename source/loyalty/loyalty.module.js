/* @flow */

import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import angularUIBootstrap from 'angular-ui-bootstrap';

import LoyaltyConfig from './loyalty.config';

import LoyaltyComponent from './loyalty.component';
import LoyaltyComponentController from './loyalty.component.controller';

export default angular.module('sf.ui.loyalty', [
  angularUIRouter,
  angularUIBootstrap,
])
  .component(LoyaltyComponent.NAME, LoyaltyComponent)
  .controller(LoyaltyComponentController.NAME, LoyaltyComponentController)
  .config(LoyaltyConfig)
  .name;
