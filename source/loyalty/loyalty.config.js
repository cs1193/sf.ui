/* @flow */
import LoyaltyComponent from './loyalty.component';

export default function LoyaltyConfig($stateProvider) {
  $stateProvider
    .state('loyalty', {
      url: '/loyalty',
      component: LoyaltyComponent.NAME,
    });
}
