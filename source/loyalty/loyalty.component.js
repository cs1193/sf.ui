import LoyaltyComponentTemplate from './loyalty.component.html';
import LoyaltyComponentController from './loyalty.component.controller';

export default {
  NAME: 'loyalty',
  template: LoyaltyComponentTemplate,
  controller: LoyaltyComponentController.NAME,
};
