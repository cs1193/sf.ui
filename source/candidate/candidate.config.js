/* @flow */
import CandidateComponent from './candidate.component';

export default function CandidateConfig($stateProvider) {
  $stateProvider
    .state('candidates', {
      url: '/candidates',
      component: CandidateComponent.NAME,
    });
}
