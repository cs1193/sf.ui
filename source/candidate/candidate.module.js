/* @flow */

import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import angularUIBootstrap from 'angular-ui-bootstrap';

import CandidateConfig from './candidate.config';

import CandidateComponent from './candidate.component';
import CandidateComponentController from './candidate.component.controller';

export default angular.module('sf.ui.candidate', [
  angularUIRouter,
  angularUIBootstrap,
])
  .component(CandidateComponent.NAME, CandidateComponent)
  .controller(CandidateComponentController.NAME, CandidateComponentController)
  .config(CandidateConfig)
  .name;
