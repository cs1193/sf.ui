/* @flow */
export default class CandidateComponentController {
  static NAME: string = 'CandidateComponentController';

  /* @ngInject */
  constructor($log) {
    this.$log = $log;
  }
}
