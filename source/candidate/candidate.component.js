import CandidateComponentTemplate from './candidate.component.html';
import CandidateComponentController from './candidate.component.controller';

export default {
  NAME: 'candidate',
  template: CandidateComponentTemplate,
  controller: CandidateComponentController.NAME,
};
