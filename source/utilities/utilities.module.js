/* @flow */

import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import angularUIBootstrap from 'angular-ui-bootstrap';

export default angular.module('sf.ui.utilities', [
  angularUIRouter,
  angularUIBootstrap,
])
  .name;
