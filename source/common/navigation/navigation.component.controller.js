/* @flow */
export default class NavigationComponentController {
  static NAME: string = 'NavigationComponentController';

  /* @ngInject */
  constructor($log) {
    this.$log = $log;
  }

  $onInit() {
    this.items = [];
  }

  addItem(item) {
    this.items.push(item);
  }

  selectItem(index) {
    for (let i = 0; i < this.items.length; i += 1) {
      this.items[i].selected = false;
    }
    this.items[index].selected = true;
  }

  $postLink() {
    this.selectItem(this.selectedItem);
  }
}
