import NavigationComponentTemplate from './navigation.component.html';

export default {
  NAME: 'navigation',
  bindings: {
    selectedItem: '@',
  },
  template: NavigationComponentTemplate,
  transclude: true,
};
