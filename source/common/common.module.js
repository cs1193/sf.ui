/* @flow */

import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import angularUIBootstrap from 'angular-ui-bootstrap';

import LayoutComponent from './layout.component';

import AppHeader from './app-header/app-header.component';

import AppFooter from './app-footer/app-footer.component';

import AppMain from './app-main/app-main.component';

import NavigationComponent from './navigation/navigation.component';
import NavigationComponentController from './navigation/navigation.component.controller';

import NavigationComponentItem from './navigation/navigation-item/navigation-item.component';

export default angular.module('sf.ui.common', [
  angularUIRouter,
  angularUIBootstrap,
])
  .component(AppHeader.NAME, AppHeader)
  .component(AppFooter.NAME, AppFooter)
  .component(AppMain.NAME, AppMain)
  .component(LayoutComponent.NAME, LayoutComponent)
  .component(NavigationComponent.NAME, NavigationComponent)
  .controller(NavigationComponentController.NAME, NavigationComponentController)
  .component(NavigationComponentItem.NAME, NavigationComponentItem)
  .name;
