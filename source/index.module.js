/* @flow */

import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import angularUIBootstrap from 'angular-ui-bootstrap';

import CommonModule from './common/common.module';
import InterviewModule from './interview/interview.module';
import CandidateModule from './candidate/candidate.module';
import LoyaltyModule from './loyalty/loyalty.module';
import UtilitiesModule from './utilities/utilities.module';

import IndexConfig from './index.config';

export default angular.module('sf.ui', [
  angularUIRouter,
  angularUIBootstrap,
  CommonModule,
  InterviewModule,
  CandidateModule,
  LoyaltyModule,
  UtilitiesModule,
])
  .config(IndexConfig)
  .name;
