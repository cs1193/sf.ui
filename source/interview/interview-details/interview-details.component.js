import InterviewDetailsComponentTemplate from './interview-details.component.html';

export default {
  NAME: 'interviewDetails',
  template: InterviewDetailsComponentTemplate,
};
