/* @flow */
import InterviewComponent from './interview.component';

export default function InterviewConfig($stateProvider) {
  $stateProvider
    .state('interviews', {
      url: '/interviews',
      component: InterviewComponent.NAME,
    });
}
