import MultipleChoiceComponentTemplate from './multiple-choice.component.html';
import MultipleChoiceComponentController from './multiple-choice.component.controller';

export default {
  NAME: 'multipleChoice',
  template: MultipleChoiceComponentTemplate,
  controller: MultipleChoiceComponentController.NAME,
};
