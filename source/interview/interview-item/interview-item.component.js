import InterviewItemComponentTemplate from './interview-item.component.html';
import InterviewItemComponentController from './interview-item.component.controller';

export default {
  NAME: 'interviewItem',
  template: InterviewItemComponentTemplate,
  controller: InterviewItemComponentController.NAME,
};
