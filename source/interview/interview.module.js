/* @flow */

import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import angularUIBootstrap from 'angular-ui-bootstrap';

import InterviewConfig from './interview.config';

import InterviewComponent from './interview.component';
import InterviewComponentController from './interview.component.controller';

import InterviewItemComponent from './interview-item/interview-item.component';
import InterviewItemComponentController from './interview-item/interview-item.component.controller';

import CreateInterviewComponent from './create-interview/create-interview.component';
import CreateInterviewComponentController from './create-interview/create-interview.component.controller';

export default angular.module('sf.ui.interview', [
  angularUIRouter,
  angularUIBootstrap,
])
  .component(InterviewComponent.NAME, InterviewComponent)
  .controller(InterviewComponentController.NAME, InterviewComponentController)
  .component(InterviewItemComponent.NAME, InterviewItemComponent)
  .controller(InterviewItemComponentController.NAME, InterviewItemComponentController)
  .component(CreateInterviewComponent.NAME, CreateInterviewComponent)
  .controller(CreateInterviewComponentController.NAME, CreateInterviewComponentController)
  .config(InterviewConfig)
  .name;
