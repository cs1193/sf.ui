/* @flow */
export default class InterviewComponentController {
  static NAME: string = 'InterviewComponentController';

  /* @ngInject */
  constructor($log) {
    this.$log = $log;
  }
}
