import CreateInterviewComponentTemplate from './create-interview.component.html';

export default {
  NAME: 'createInterview',
  template: CreateInterviewComponentTemplate,
};
