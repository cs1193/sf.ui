/* @flow */
export default class CreateInterviewComponentController {
  static NAME: string = 'CreateInterviewComponentController';

  /* @ngInject */
  constructor($log) {
    this.$log = $log;
  }
}
