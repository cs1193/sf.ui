import InterviewComponentTemplate from './interview.component.html';
import InterviewComponentController from './interview.component.controller';

export default {
  NAME: 'interview',
  template: InterviewComponentTemplate,
  controller: InterviewComponentController.NAME,
};
